package com.company.Model;

public class Matches {

    private int matches;

    public Matches(int matches) {
        this.matches = matches;
    }

    public int getMatches() {
        return matches;
    }

    public void addMatch(int matches) {
        this.matches += matches;
    }
}
