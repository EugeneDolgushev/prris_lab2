package com.company.Model;

import java.util.Iterator;
import java.util.TreeSet;

public class ContactsByLetter implements Comparable<ContactsByLetter> {

    private String firstLetter;
    private TreeSet<String> names;

    public ContactsByLetter(String letter) {
        firstLetter = letter;
        names = new TreeSet<>();
    }

    public String getFirstLetter() {
        return firstLetter;
    }

    public TreeSet<String> getNames() {
        return names;
    }

    public void addName(String name) {
        if (!names.add(name)) {
            throw new RuntimeException(String.format("Name: %s is already exists", name));
        }
    }

    public int findName(String name) {
        String[] strNames = convert();
        int position = findFirstNameMatch(strNames, name);
        if (position == -1) {
            return 0;
        }
        Matches matches = new Matches(1);
        findNameMatchAbove(strNames, position - 1, name, matches);
        findNameMatchBelow(strNames, position + 1, name, matches);
        return matches.getMatches();
    }

    /*
     * Возвращает позицию первого найденного совпадения по словам
     * */
    private int findFirstNameMatch(String[] strNames, String name) {
        int first = 0, last = strNames.length - 1;
        int position = (first + last) / 2;
        int letterPosition = 1;

        //(!strNames[position].equals(name)) &&
        while ((first <= last)) {
            String existingName = strNames[position];
            if (letterPosition >= name.length()) {
                break;
            }
            if (name.charAt(letterPosition) < existingName.charAt(letterPosition)) {
                last = position - 1;
            } else if (name.charAt(letterPosition) > existingName.charAt(letterPosition)) {
                first = position + 1;
            } else {
                ++letterPosition;
            }
            position = (first + last) / 2;
        }

        if (letterPosition < name.length() - 1) {
            return -1;
        }
        return position;
    }

    /*
     * Ищет совпадения вверх по списку контактов
     * */
    private void findNameMatchAbove(String[] strNames, int position, String name, Matches matches) {
        if (position < 0) {
            return;
        }
        if (strNames[position].substring(0, name.length()).equals(name)) {
            matches.addMatch(1);
            findNameMatchAbove(strNames, position - 1, name, matches);
        }
    }

    /*
     * Ищет совпадения вниз по списку контактов
     * */
    private void findNameMatchBelow(String[] strNames, int position, String name, Matches matches) {
        if (position > strNames.length - 1) {
            return;
        }
        if (strNames[position].substring(0, name.length()).equals(name)) {
            matches.addMatch(1);
            findNameMatchBelow(strNames, position + 1, name, matches);
        }
    }

    private String[] convert() {
        String[] names = new String[this.names.size()];
        Iterator<String> iterator = this.names.iterator();
        int count = 0;
        while (iterator.hasNext()) {
            names[count] = iterator.next();
            ++count;
        }

        return names;
    }

    @Override
    public int compareTo(ContactsByLetter o) {
        return this.firstLetter.compareTo(o.getFirstLetter());
    }
}
