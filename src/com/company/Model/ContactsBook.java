package com.company.Model;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

public class ContactsBook {

    private Map<String, Integer> cashedResults;
    private TreeSet<ContactsByLetter> contactsByLetters;

    public ContactsBook() {
        contactsByLetters = new TreeSet<>();
        cashedResults = new HashMap<>();
    }

    public TreeSet<ContactsByLetter> getContactsByLetters() {
        return contactsByLetters;
    }

    public int findName(String name) {
        if (cashedResults.containsKey(name)) {
            return cashedResults.get(name);
        } else {
            String firstLetter = name.substring(0, 1);
            for (ContactsByLetter contactsByLetter : contactsByLetters) {
                if (firstLetter.equals(contactsByLetter.getFirstLetter())) {
                    int matches = contactsByLetter.findName(name);
                    cashedResults.put(name, matches);
                    return matches;
                }
            }
            return -1;
        }
    }

    public void addName(String name) {
        updateCashedResults(name);
        if (contactsByLetters.isEmpty()) {
            addNewName(name.substring(0, 1), name);
        } else {
            String firstLetter = name.substring(0, 1);
            boolean isLetterExists = false;
            for (ContactsByLetter contactsByLetter : contactsByLetters) {
                if (firstLetter.equals(contactsByLetter.getFirstLetter())) {
                    contactsByLetter.addName(name);
                    isLetterExists = true;
                    break;
                }
            }
            if (!isLetterExists) {
                addNewName(firstLetter, name);
            }
        }
    }

    private void addNewName(String firstLetter, String name) {
        ContactsByLetter contactsByLetter = new ContactsByLetter(firstLetter);
        contactsByLetter.addName(name);
        contactsByLetters.add(contactsByLetter);
    }

    private void updateCashedResults(String name) {
        new Thread(() -> {
            for (String key : cashedResults.keySet()) {
                if (name.contains(key) && name.indexOf(key) == 0) {
                    cashedResults.replace(key, cashedResults.get(key) + 1);
                }
            }
        }).start();
    }
}
