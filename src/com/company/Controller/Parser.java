package com.company.Controller;

public class Parser {

    public static String parseAction(String input) {
        if (input.isEmpty()) {
            throw new RuntimeException("Empty input");
        }
        if (input.indexOf(' ') == -1) {
            throw new RuntimeException("Illegal input");
        }
        return input.substring(0, input.indexOf(' '));
    }

    public static String parseName(String input, String action) {
        if (input.isEmpty()) {
            throw new RuntimeException("Empty input");
        }
        if (input.length() == action.length() + 1) {
            throw new RuntimeException("Illegal input");
        }
        String name = input.substring(action.length()).trim();
        if (name.isEmpty()) {
            throw new RuntimeException("Empty name");
        }
        return name;
    }
}
