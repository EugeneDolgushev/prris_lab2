package com.company.Controller;

import com.company.Model.ContactsBook;
import com.company.Model.ContactsByLetter;

import java.io.InputStream;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        InputStream source = System.in;
        Scanner scanner = new Scanner(source);
        ContactsBook contactsBook = new ContactsBook();
        while (scanner.hasNext()) {
            String input = scanner.nextLine();
            if (!input.isEmpty()) {
                try {
                    String action = Parser.parseAction(input);
                    String name = Parser.parseName(input, action);
                    switch (action) {
                        case "add":
                            contactsBook.addName(name);
                            System.out.println("Contact added");
                            break;
                        case "find":
                            int matches = contactsBook.findName(name);
                            if (matches <= 0) {
                                System.out.printf("No contacts with: %s%n", name);
                            } else {
                                System.out.printf("Find: %s -> %d%n", name, matches);
                            }
                            break;
                        default:
                            System.out.println("Unknown command");
                            break;
                    }
                } catch (RuntimeException e) {
                    System.out.println(e.toString());
                }
            }
        }
    }

    private static void testAddNameToContactsByLetter() {
        ContactsByLetter contactsByLetter = new ContactsByLetter("a");
        try {
            contactsByLetter.addName("alex");
            contactsByLetter.addName("alex");
            contactsByLetter.addName("alexey");
            contactsByLetter.addName("alexander");
            contactsByLetter.addName("alexynder");
            contactsByLetter.addName("alexozavr");
        } catch (RuntimeException e) {
            System.out.println(e.toString());
        }
        System.out.println("First letter: " + contactsByLetter.getFirstLetter());
        for (String s : contactsByLetter.getNames()) {
            System.out.println(s);
        }
    }

    private static void testAddNameToContactBook() {
        ContactsBook contactsBook = new ContactsBook();
        contactsBook.addName("alex");
        for (ContactsByLetter contactsByLetter : contactsBook.getContactsByLetters()) {
            System.out.println("First letter: " + contactsByLetter.getFirstLetter());
            for (String name : contactsByLetter.getNames()) {
                System.out.println(name);
            }
        }
    }

    private static void testAddNamesToContactBook() {
        ContactsBook contactsBook = new ContactsBook();
        contactsBook.addName("alex");
        contactsBook.addName("monica");
        contactsBook.addName("steven");
        contactsBook.addName("brittie");
        contactsBook.addName("andrey");
        contactsBook.addName("bob");
        contactsBook.addName("polly");
        contactsBook.addName("nancy");
        contactsBook.addName("stacy");
        contactsBook.addName("bruce");
        contactsBook.addName("andy");
        contactsBook.addName("odri");
        contactsBook.addName("raven");
        contactsBook.addName("scott");
        for (ContactsByLetter contactsByLetter : contactsBook.getContactsByLetters()) {
            System.out.println("First letter: " + contactsByLetter.getFirstLetter());
            for (String name : contactsByLetter.getNames()) {
                System.out.println(name);
            }
        }
    }

    private static void testFindNameInContactBook() {
        ContactsBook contactsBook = new ContactsBook();
        contactsBook.addName("alex");
        contactsBook.addName("alexander");
        contactsBook.addName("alexey");
        contactsBook.addName("alexynder");
        contactsBook.addName("andron");
        contactsBook.addName("andry");
        contactsBook.addName("monica");
        contactsBook.addName("steven");
        contactsBook.addName("brittie");
        contactsBook.addName("andrey");
        contactsBook.addName("bob");
        contactsBook.addName("polly");
        contactsBook.addName("nancy");
        contactsBook.addName("stacy");
        contactsBook.addName("bruce");
        contactsBook.addName("andy");
        contactsBook.addName("odri");
        contactsBook.addName("raven");
        contactsBook.addName("scott");
        System.out.println(contactsBook.findName("alex"));
    }
}
